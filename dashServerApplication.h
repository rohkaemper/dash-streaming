#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/log.h"

#include <queue>

using namespace ns3;
using namespace std;


// namespace ns3 {
// NS_LOG_COMPONENT_DEFINE ("DashServerApplication");

class DashServerApplication : public Application {
  public:
    DashServerApplication ();
    virtual ~DashServerApplication ();
    void Setup (Ptr<Socket> socket, DataRate datarate, Address localAddress);

  private:
    virtual void StartApplication (void);
    virtual void StopApplication (void);
    void HandleAccept (Ptr<Socket> socket, const Address& from);
    void HandlePeerClose (Ptr<Socket> socket);
    void HandlePeerError (Ptr<Socket> socket);

    void ReceivePacket (Ptr<Socket> socket);
    void DeliverStream (Ptr<Socket> socket, uint32_t txSpace);

    // Ptr<Socket> getSocket ();
    // Ptr<Socket> setSocket ();
    void logMsg (string msg);
    void logMsg (int msg);
    void print_queue(std::queue<unsigned long> q);

    //Variables
    Ptr<Socket>           m_socket;
    list<Ptr<Socket> >    m_socketlist;
    DataRate              m_datarate;
    Address               m_address;
    uint32_t              m_segmentsize;
    std::queue <unsigned long>  m_queue;
    bool                  m_active;
    uint32_t              m_txbytes;

    bool                  m_showDebug;

};


DashServerApplication::DashServerApplication ()
  : m_socket (0),
    m_socketlist (),
    m_datarate (0),
    m_address (),
    m_segmentsize (0),
    m_queue (),
    m_active (false),
    m_txbytes (0),
    m_showDebug (true)
{
}


DashServerApplication::~DashServerApplication() {
  if (m_socket) {
    m_socket->Close ();
  }
  m_socket = 0;

}


void DashServerApplication::Setup (Ptr<Socket> socket, DataRate datarate, Address localAddress) {
  m_socket = socket;
  m_datarate = datarate;
  m_address = localAddress;

}


void DashServerApplication::StartApplication (void) {
  m_socket->Bind (m_address);
  m_socket->Listen ();

  m_socket->SetRecvCallback(MakeCallback (&DashServerApplication::ReceivePacket, this));

  m_socket->SetAcceptCallback(MakeNullCallback <bool, Ptr<Socket>, const Address&> (), MakeCallback(&DashServerApplication::HandleAccept, this));
  m_socket->SetCloseCallbacks (MakeCallback (&DashServerApplication::HandlePeerClose, this), MakeCallback (&DashServerApplication::HandlePeerError, this));

  // logMsg("Server has been started and is listening!");
}


void DashServerApplication::StopApplication (void) {
  m_active = false;
  while (!m_socketlist.empty ()) {
    Ptr<Socket> acceptedSocket = m_socketlist.front ();
    m_socketlist.pop_front ();
    acceptedSocket->Close ();
  }

  if (m_socket) {
    m_socket->Close ();
    m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
  }
  // logMsg("Server has been stopped and all sockets are closed.");
}


/*
 * Receiving and delivering packets...
 */
void DashServerApplication::ReceivePacket (Ptr<Socket> socket) {

  Ptr<Packet> packet = socket->Recv();
  uint8_t *buffer = new uint8_t[packet->GetSize ()];
  packet->CopyData (buffer, packet->GetSize ());
  string size = string ((char*) buffer);

  // create the segment and push it to the queue
  uint32_t segmentsize = std::stoi(size);
  // logMsg("Received request for " + to_string(segmentsize) + " bits.");
  if (segmentsize > 0) {
    m_queue.push(segmentsize);
    logMsg("SERVER-> Segment [" + size + "] enqueued.");

    // DEBUG: print queue elements
    // TODO(sr): Find out why many elements in queue and none deleted after transmission.
    // std::queue<uint32_t> printVersion = m_queue;
    // print_queue(printVersion);
    DeliverStream(socket, socket->GetTxAvailable());

  } else {
    // logMsg ("SERVER-> ERROR invalid segmentsize (<0). [" + to_string(segmentsize) + "]");
  }

}


/* TODO: HIER WEITER TX PROBLEME / MENGEN STIMMEN NICHT ÜBEREIN GLAUBE ICH */
void DashServerApplication::DeliverStream (Ptr<Socket> socket, uint32_t txSpace) {
  // logMsg ("SERVER-> Packet stream function.");
  while (!m_queue.empty()) {
    int bytes;
    Ptr<Packet> packet = Create<Packet> (m_queue.front());
    if (bytes += socket-> Send(packet) != m_queue.front()) {
      break;
    }
    m_queue.pop();
  }

  // while (m_txbytes < m_queue.front()) {
  //   uint32_t left = m_queue.front() - m_txbytes;
  //   int amountSent = socket->Send (packet);

  //   if (amountSent < 0) {
  //     return;
  //   }
  //   m_txbytes += amountSent;
  //   logMsg("tx:\d" + to_string(m_txbytes));
  // }
  // m_txbytes = 0;
}


/*
 * Setting up callbacks...
 */
void DashServerApplication::HandleAccept (Ptr<Socket> socket, const Address& from) {
  socket->SetRecvCallback (MakeCallback (&DashServerApplication::ReceivePacket, this));
  // logMsg ("SERVER-> Receive callback set.");
  socket->SetSendCallback(MakeCallback(&DashServerApplication::DeliverStream, this));
  // logMsg ("SERVER-> Send callback set.");

  m_socketlist.push_back (socket);
  // logMsg ("SERVER-> Connection Accepted.");

}


void DashServerApplication::HandlePeerClose (Ptr<Socket> socket) {
  // logMsg("SERVER-> Peer Close callback set.");

}


void DashServerApplication::HandlePeerError (Ptr<Socket> socket) {
  // logMsg("SERVER-> Peer Error callback set.");

}


/*
 * logging functionality to debug in cli
 */
void DashServerApplication::logMsg (string msg) {
  if (m_showDebug) {
    NS_LOG_UNCOND (msg);
  }

}


void DashServerApplication::logMsg (int msg) {
  if (m_showDebug) {
    NS_LOG_UNCOND (to_string(msg));
  }

}

void DashServerApplication::print_queue(std::queue<unsigned long> q)
{
  std::cout << "QUEUE: ";
  while (!q.empty())
  {
    std::cout << q.front() << " ";
    q.pop();
  }
  std::cout << std::endl;
}
// } // END NAMESPACE
